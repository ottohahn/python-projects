#!/usr/bin/env python
"""
      *+---------------------------------------------------------------+
      *|                                                               |
      *|      NAME: calcvoldb.py                                       |
      *|   PURPOSE: Calculate Database volumes for different number of |
      *|            records                                            |
      *|    AUTHOR: OTTO HAHN HERRERA                                  |
      *|      DATE: 2018-05-31                                         |
      *| CHANGELOG: 2018-05-31 | CREATION                              |
      *|                                                               |
      *+---------------------------------------------------------------+
"""

import argparse
# import os
import csv
import pytablewriter


def calculate_size(field_tuple):
    """
    A FUNCTION TO CALCULATE THE SIZE OF A FIELD
    GIVEN THE TYPE, LENGTH AND PRECISION
    WE SSUME A 500 LONG 16 BITS ENCODED LENGTH FOR TEXT AND JSON
    """
    # THE FOLLOWING SECTION CODES THE BYTE SIZES OF THE DIFFERENT DATA TYPES
    # IN POSTGRESQL
    size = 0.0
    SIZES = {
        'SMALLINT':2,
        'INT':4,
        'BIGINT':8,
        'DECIMAL':11,
        'NUMERIC':11,
        'REAL':4,
        'DOUBLE PRECISION':8,
        'SMALLSERIAL':2,
        'SERIAL':4,
        'BIGSERIAL':8,
        'MONEY': 8,
        'VARCHAR': 1,
        'TEXT':16,
        'JSON':16,
        'CHAR':1,
        'BYTEA':1,
        'TIME':8,
        'DATE':4,
        'TIMESTAMP':8,
        'BOOL':1
        }
    
    OVERHEADS = {
        'SMALLINT':0,
        'INT':0,
        'BIGINT':0,
        'DECIMAL':0.5,
        'NUMERIC':0.5,
        'REAL':0,
        'DOUBLE PRECISION':0,
        'SMALLSERIAL':0,
        'SERIAL':0,
        'BIGSERIAL':0,
        'MONEY': 0,
        'VARCHAR':4,
        'TEXT':0,
        'JSON':0,
        'CHAR':4,
        'BYTEA':0,
        'TIME':0,
        'DATE':0,
        'TIMESTAMP':0,
        'BOOL':0
        }

    if field_tuple[0] in ['DECIMAL', 'NUMERIC']:
        size = 11 + int(float(field_tuple[1]) * OVERHEADS[field_tuple[0]])
    elif field_tuple[0] in ['CHAR', 'VARCHAR']:
        size = (int(field_tuple[1]) * SIZES[field_tuple[0]]) + OVERHEADS[field_tuple[0]]
    elif field_tuple[0] in ['TEXT', 'JSON']:
        SIZES[field_tuple[0]] * 500
    else:
        size = SIZES[field_tuple[0]] + OVERHEADS[field_tuple[0]]
    return size

# PARSER ARGUMENTS SECTION

PARSER = argparse.ArgumentParser(
    description='Calculate database size',
    epilog="""Size is calculated using 10, 100, 1000, 100,000, 1,000,000
              rows per table for your estimated calculation you can then
              multiply the values for the required number of records""")
PARSER.add_argument('--file', '-f', help='name of input file table,field,type,len')
PARSER.add_argument('--out', '-o', help='name of output file (Markdown formatted)')


# INPUT SECTION

ARGS = PARSER.parse_args()
INFILENAME = ARGS.file
OUTFILENAME = ARGS.out

tables = {}

# PROCESSING SECTION
i = 0
with open(INFILENAME) as csvfile:
    SPAMREADER = csv.reader(csvfile, delimiter=',')
    for row in SPAMREADER:
        print(i, row)
        if row[0] not in tables:
            tables[row[0]] = [(row[2], row[3], row[4])]
        else:
            tables[row[0]].append((row[2], row[3], row[4]))
        i += 1
results = []

for table in tables.keys():
    size = 0.0
    for field in tables[table]:
        size += calculate_size(field)
    size10 = size * 10
    size100 = size * 100
    size1k = size * 1000
    size10k = size * 10000
    size100k = size * 100000
    size1M = size * 1000000
    results.append([table, size10, size100, size1k, size10k, size100k, size1M])

# Output section

WRITER = pytablewriter.MarkdownTableWriter()
WRITER.table_name = "THEORETICAL VOLUME RESULTS"
WRITER.header_list = ["TABLE", "10 rpt", "100 rpt", "1000 rpt",
                      "10,000 rpt", "10E5 rpt", "10E6 rpt"]
WRITER.value_matrix = results
WRITER.margin = 1  # add a whitespace for both sides of each cell
WRITER.stream = open(OUTFILENAME, 'w+')
WRITER.write_table()
