#!/usr/bin/env python

"""
Filename: hello_world.py
Author: Otto Hahn Herrera
Purpose: To introduce python to the reader
Date: 2024-01-14
"""

print('Hello World!')
