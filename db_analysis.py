#!/usr/bin/env python

"""
extractschema.py extracts the schema of a MySQL database, prints a list
of tables, and for each table the fields and types.
"""
import MySQLdb as db
import pandas as pd
import pytablewriter

dbname = 'testdb'

writer = pytablewriter.MarkdownTableWriter()
conn = db.connect(user='otto', passwd='ottoman', db=dbname)

tables = pd.read_sql('show tables' ,conn)

writer.table_name = 'DB Schema of '+dbname
writer.header_list = list(tables.columns.values)
writer.value_matrix = tables.values.tolist()
writer.write_table()

table_list_str = 'Tables_in_'+dbname

for i in tables[table_list_str]:
    writer.table_name = "Table structure "+i
    q_schema ="describe "+i
    df_schema = pd.read_sql(q_schema ,conn)
    writer.header_list = list(df_schema.columns.values)
    writer.value_matrix = df_schema.values.tolist()
    writer.write_table()
    q_table = "select * from "+i
    # iterate over the fields in each table
    print("### Univariate analysis by column")
    df_table = pd.read_sql(q_table, conn)
    columns = df_table.columns.tolist()
    for col in columns:
        if (df_table[col].dtype == "object"):
            try:
                # Try to coerce conversion to date
                df_table[col] = pd.to_datetime(df_table[col], format='%Y/%m/%d')
                print('Conversion exitosa',df_table[col].dtype)
                #writer.table_name = 'Counts of  '+col
                counts = df_table[col].value_counts(dropna=False)
                counts_df = pd.DataFrame({'Objects':counts.index, 'Counts': counts.values})
                counts_df['New_ID'] = list(range(0, counts_df.shape[0]))
                counts_df = counts_df.set_index('New_ID')
                print(counts_df)
                #writer.from_dataframe(counts_df)
                #writer.value_matrix = counts_df.values.tolist()  ## TO DO
                #writer.write_table()
                print("- Minimum", counts_df['Objects'].min())
                print("- Maximum", counts_df['Objects'].max())
            except ValueError:
                print('Conversion fallida', df_table[col].dtype)
                pass
                # If we miserably fail, then do the counts then trim the string to 256 chars and plot
                writer.table_name = 'Counts of  '+col
                writer.header_list = ['Objects', 'Counts']
                counts = df_table[col].value_counts(dropna=False)
                # The counts are OK up to here, but we need to trim the long strings
                non_trimmed_idx = counts.index.tolist()
                trimmed_idx = []
                for long_string in non_trimmed_idx:
                    trimmed_idx.append(long_string[:50])
                counts_df = pd.DataFrame({'Objects':counts.values, 'Counts': trimmed_idx})
                writer.value_matrix = counts_df.values.tolist()
                writer.write_table()
        else:
            print("****")
            writer.table_name = 'Counts of  '+col
            writer.header_list = ['Objects', 'Counts']
            counts = df_table[col].value_counts(dropna=False)
            counts_df = pd.DataFrame({'Objects':counts.values, 'Counts': counts.index})
            writer.value_matrix = counts_df.values.tolist()
            writer.write_table()
            #print(df_table[col].value_counts(dropna=False))
            print("****")
            if df_table[col].dtype == "float64":
                print("- Minimum", df_table[col].min())
                print("- Maximum", df_table[col].max())
                print("- Mean", df_table[col].mean())
                print("- Median", df_table[col].median())
            elif df_table[col].dtype == "int64":
                print("- Minimum", df_table[col].min())
                print("- Maximum", df_table[col].max())
            