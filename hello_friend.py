#!/usr/bin/env python
"""
 Filename: hello_friend.py
 Author: Otto Hahn Herrera
 Purpose: To introduce user input in python and basic validation with if clause
 Date: 2024-01-14
 """
friend = input('Hello! What is your name? ') 
if friend in ('', ' '):
    print('Hello friend!')
else:
    print(f'Hello {friend}!')       
