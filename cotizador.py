#!/usr/bin/env python


def cotizador(tipo_app):
    """
    input el tipo de app, luego de manera interactiva calcula los costos
    el costo por feature son 625.00 MXN
    el costo de diseño es 2000.00 MXN
    el costo de una DB son 10000.00
    """
    if tipo_app == 'db':
        costo = 10000.00
    if tipo_app == 'elastic':
        kb = 5000.0
        costo = 625.00
        dis = input("Tienes el diseño de tu página? (s/n) ")
        if dis == "n":
            dis_p = 1500.00
        else:
            dis_p = 0.0
        num = int(input("Cuántas páginas necesitas? "))
        num_f = int(input("cuantas unidades funcionales necesitas? "))
        costo = costo * num + costo * num_f + dis_p + kb
    if tipo_app == 'web':
        costo = 500.0
        dis = input("Tienes el diseño de tu página? (s/n) ")
        if dis == "n":
            dis_p = 1000.00
        else:
            dis_p = 0.0
        num = int(input("Cuántas páginas necesitas? "))
        costo = (num * costo) + dis_p
    if tipo_app == 'webapp':
        costo = 625.00
        dis = input("Tienes el diseño de tu página? (s/n) ")
        if dis == "n":
            dis_p = 1500.00
        else:
            dis_p = 0.0
        num = int(input("Cuántas páginas necesitas? "))
        num_f = int(input("cuantas unidades funcionales necesitas? "))
        costo = costo * num + costo * num_f + dis_p
    if tipo_app == 'analisis':
        tab_o_text = input("son tablas o texto? ")
        if tab_o_text == "tablas":
            costo = 20.0
        elif tab_o_text == "texto":
            costo = 25.0
        num_texts = int(input("Cual es el número de documentos? "))
        costo = costo * num_texts
    if tipo_app == 'custom':
        num_horas = int(input("Por favor ingrese el número de horas estimadas"))
        costo = num_horas * 500
    return costo

if __name__=='__main__':
    print("COTYzador de proyectos") 
    tipo=input("""inserte tipo de proyecto:
                   analisis
                   db
                   elastic
                   web
                   webapp
                   custom
                  NOTA: una unidad funcional, es una forma, botón, descarga de archivo, endpoint, ...
                    """)
    print("Costo:",cotizador(tipo))