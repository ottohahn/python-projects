#!/usr/bin/env python
"""
 Filename: madlibs.py
 Author: Otto Hahn Herrera
 Purpose: Program to play madlibs using f strings
 Date: 2024-01-25
"""

print("I'll show you a madlib and you can fill it with the different parts of \
        speech") 
madlib = """
Today I went to the zoo. I saw a(n)
___________(adjective)
_____________(Noun) jumping up and down in its tree.
He _____________(verb, past tense) __________(adverb)
through the large tunnel that led to its _______(adjective)
__________(noun). I got some peanuts and passed
them through the cage to a gigantic gray _______(noun)
towering above my head. Feeding that animal made
me hungry. I went to get a __________(adjective) scoop
of ice cream. It filled my stomach. Afterwards I had to
__________(verb) __________ (adverb) to catch our bus.
When I got home I __________(verb, past tense) my
mom for a __________(adjective) day at the zoo.
"""
# print original

print(madlib)

# get input from user

print("fill the parts of speech needed")
adj1 = input('adjective: ')
noun1 = input('noun: ')
verb1 = input('verb, past tense: ')
adverb1 = input('adverb: ')
adj2 = input('adjective: ')
noun2 = input('noun: ')
noun3 = input('noun: ')
adj3 = input('adjective: ')
verb2 = input('verb: ')
adverb2 = input('adverb: ')
verb3 = input('verb, past tense: ')
adj4 = input('adjective: ')

# fill the blanks

fmadlib = f"""
Today I went to the zoo. I saw a(n)                                              
{adj1}                                                           
{noun1} jumping up and down in its tree.                             
He {verb1} {adverb1}                            
through the large tunnel that led to its {adj2}                      
{noun2}. I got some peanuts and passed                                  
them through the cage to a gigantic gray {noun3}                           
towering above my head. Feeding that animal made                                 
me hungry. I went to get a {adj3} scoop                           
of ice cream. It filled my stomach. Afterwards I had to                          
{verb2} {adverb2} to catch our bus.                           
When I got home I {verb3} my                                
mom for a {adj4} day at the zoo.                                  
"""                                            
# print result
print("This is your result")
print(fmadlib)

