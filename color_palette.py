import cv2
import numpy as np
from sklearn.cluster import KMeans
from skimage.color import rgb2lab, deltaE_cie76
from collections import Counter
import webcolors

def get_palette(image_path, n_colors=5):
    # Load the image
    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # Flatten the image to a list of RGB pixels
    pixels = image.reshape(-1, 3)

    # Convert RGB to CIELab color space
    pixels_lab = rgb2lab(pixels)

    # Perform k-means clustering to find dominant colors
    kmeans = KMeans(n_clusters=n_colors)
    kmeans.fit(pixels_lab)
    cluster_centers_lab = kmeans.cluster_centers_

    # Convert cluster centers back to RGB
    cluster_centers_rgb = lab2rgb(cluster_centers_lab)

    # Get the hex values of the cluster colors
    palette = []
    for color in cluster_centers_rgb:
        hex_color = rgb_to_hex(color)
        palette.append(hex_color)

    # Sort the palette by the number of pixels in each cluster (dominance)
    color_counts = Counter(kmeans.labels_)
    palette = sorted(palette, key=lambda color: -color_counts[kmeans.predict(np.array(hex_to_rgb(color)).reshape(1, -1))[0]])

    return palette

def rgb_to_lab(rgb):
    return rgb2lab(np.array([[rgb]])).reshape(-1, 3)

def lab2rgb(lab):
    return cv2.cvtColor(lab.reshape(-1, 1, 3), cv2.COLOR_LAB2RGB).reshape(-1, 3)

def rgb_to_hex(rgb):
    return '#{:02X}{:02X}{:02X}'.format(int(rgb[0]), int(rgb[1]), int(rgb[2]))

def hex_to_rgb(hex_color):
    hex_color = hex_color.lstrip('#')
    return tuple(int(hex_color[i:i+2], 16) for i in (0, 2, 4))

if __name__ == "__main__":
    image_path = "your_image.jpg"  # Replace with the path to your image
    n_colors = 5  # Number of dominant colors in the palette

    palette = get_palette(image_path, n_colors)

    print("Generated Palette:")
    for color in palette:
        print(color)

