#!/usr/bin/env python
"""
 Filename: caesar_cypher.py
 Author: Otto Hahn Herrera
 Purpose: To generate the Caesar cypher and use string manipulation
 Date: 2024-01-20
"""

alp = 'abcdefghijklmnopqrstuvwxyz'
key = 3
plaintext = 'This is a test message'
preproc = plaintext.lower()
print('plaintext: ', plaintext)
print('preprocessed text: ', preproc)
code = ''
for l in preproc:
    if l in alp:
        o = alp.index(l) 
        n = (alp.index(l) + key) % 26
        c = alp[n]
    else:
      c = l
    code += c
print('coded text: ',code)

