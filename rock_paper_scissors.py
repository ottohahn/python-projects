#!/usr/bin/env python
"""
 Filename: rock_paper_scissors.py
 Author: Otto Hahn Herrera
 Purpose: Program to play rock paper scissors using random choices
 Date: 2024-01-25
"""
import random as rnd

print("Rock, paper, scissors")
print("play to best of 3 or enter q to quit")
i = 0
pl = ''
score_player = 0
score_computer = 0
while (i < 3):
    pl = input('Enter your choice (r|p|s): ')
    if pl == 'q':
        break
    comp = rnd.choice(['r','p','s' ])
    print('player: ', pl, 'computer: ', comp)
    if ((pl=='r' and comp=='s') or (pl=='p' and comp=='r') or (pl=='s' and comp\
        =='p')): 
        score_player += 1
    elif ((comp=='r' and pl=='s') or (comp=='p' and pl=='r') or (comp=='s' and \
            pl =='p')):
        score_computer += 1   
    i += 1
if score_player > score_computer:
    print('Player wins!!')

elif score_computer > score_player:
    print('Computer wins!!')

else:
    print("It's a tie")

